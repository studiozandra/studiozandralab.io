const urlsList = [
    ["Run the race with endurance", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201410_1_VIDEO"],
    ["Jehovah loves the humble ones", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201410_2_VIDEO"],
    ["Groaning will be turned into rejoicing", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201410_3_VIDEO"],
    ["Curb wrong desires immediately", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201411_1_VIDEO"],
    ["We are not ignorant of his designs", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201411_2_VIDEO"],
    ["Beware of overconfidence", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201412_1_VIDEO"],
    ["Remove the roadblock to forgiveness", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201412_2_VIDEO"],
    ["Do I conduct myself as a lesser one", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201412_3_VIDEO"],
    ["Older ones-- ever faithful", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201501_1_VIDEO"],
    ["Happy is that slave", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201501_2_VIDEO"],
    ["The power of example", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201501_3_VIDEO"],
    ["Walk as wise persons", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201501_4_VIDEO"],
    ["Marry only in the lord", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201501_5_VIDEO"],
    ["Unity breaks down barriers", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201502_1_VIDEO"],
    ["Reflect like mirrors Jehovah's Glory", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201502_2_VIDEO"],
    ["What is faith", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201502_3_VIDEO"],
    ["Counsel that benefits us", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201502_4_VIDEO"],
    ["Maintain the oneness of the spirit", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201502_5_VIDEO"],
    ["He will get up again", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201503_1_VIDEO"],
    ["Do we appreciate our meetings?", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201503_2_VIDEO"],
    ["Enduring the imperfections of others", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201504_1_VIDEO"],
    ["Jehovah's day is coming as a thief", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201504_2_VIDEO"],
    ["Be willing to open your hand", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201505_1_VIDEO"],
    ["The slave is not 1900 years old", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201506_1_VIDEO"],
    ["Do not be misled by the wicked", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201506_2_VIDEO"],
    ["Consider others superior to you", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201507_1_VIDEO"],
    ["His delight is in the law", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201507_2_VIDEO"],
    ["Do not love the world", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201508_1_VIDEO"],
    ["I delight in the law of God", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201508_2_VIDEO"],
    ["Jehovah blesses obedience", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201509_1_VIDEO"],
    ["Hezekiah's secret weapon", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201510_1_VIDEO"],
    ["How can we be fair?", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201510_2_VIDEO"],
    ["Let my Cry for help reach you", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201511_1_VIDEO"],
    ["You created all things", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201511_2_VIDEO"],
    ["The generation will not pass away", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201511_3_VIDEO"],
    ["He fell in love with her", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201512_1_VIDEO"],
    ["When you see all these things", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201512_2_VIDEO"],
    ["Because you are righteous", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201512_3_VIDEO"],
    ["Jehovah wants us to be generous", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201601_1_VIDEO"],
    ["Develop the tongue of the wise ones", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201602_1_VIDEO"],
    ["Beware of overconfidence", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201602_2_VIDEO"],
    ["Young ones are precious in Jah's eyes", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201602_3_VIDEO"],
    ["A hiding place from the wind", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201603_1_VIDEO"],
    ["Our faithful older ones", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201603_2_VIDEO"],
    ["Fighting the war inside us", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201603_3_VIDEO"],
    ["Keep your eyes loyal to Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201604_1_VIDEO"],
    ["Accept change with faith and confidence", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201604_2_VIDEO"],
    ["Practice godly devotion in your own household", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201605_1_VIDEO"],
    ["Jehovah does not forget your love", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201605_2_VIDEO"],
    ["Do Not Hold a Grudge", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201606_1_VIDEO"],
    ["Can we speed up his day?", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201607_1_VIDEO"],
    ["Beware of Deceit", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201607_2_VIDEO"],
    ["Decisions of the faithful and discreet slave", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201608_1_VIDEO"],
    ["Slave for Jehovah--a cherished privilege", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201608_2_VIDEO"],
    ["Win the inner struggle", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201609_1_VIDEO"],
    ["Never be a cause for stumbling", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201610_1_VIDEO"],
    ["A word spoken at the right time", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201610_2_VIDEO"],
    ["Taste the ransom", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201611_1_VIDEO"],
    ["Keep perceiving Jehovah's will", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201611_2_VIDEO"],
    ["Don’t limit your potential", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201612_1_VIDEO"],
    ["Show favor to lowly ones", "https://tv.jw.org/#en/mediaitems/pub-jwbmw_201612_2_VIDEO"],
    ["The members….are necessary", "https://tv.jw.org/#en/mediaitems/pub-jwb_201701_11_VIDEO"],
    ["Maintain a Waiting attitude", "https://tv.jw.org/#en/mediaitems/pub-jwb_201701_12_VIDEO"],
    ["Reaching the Most distant part", "https://tv.jw.org/#en/mediaitems/pub-jwb_201701_9_VIDEO"],
    ["Pray for the peace of God", "https://tv.jw.org/#en/mediaitems/pub-jwb_201702_10_VIDEO"],
    ["Why is neutrality so important?", "https://tv.jw.org/#en/mediaitems/pub-jwb_201702_6_VIDEO"],
    ["Jehovah supports the sick", "https://tv.jw.org/#en/mediaitems/pub-jwb_201703_11_VIDEO"],
    ["Be flexible for the good news", "https://tv.jw.org/#en/mediaitems/pub-jwb_201703_12_VIDEO"],
    ["Jehovah guides his people", "https://tv.jw.org/#en/mediaitems/pub-jwb_201703_13_VIDEO"],
    ["Deep study draws us close to Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201704_11_VIDEO"],
    ["Seek first the kingdom", "https://tv.jw.org/#en/mediaitems/pub-jwb_201704_7_VIDEO"],
    ["Pray for integrity", "https://tv.jw.org/#en/mediaitems/pub-jwb_201705_11_VIDEO"],
    ["Jesus fulfilled the law", "https://tv.jw.org/#en/mediaitems/pub-jwb_201705_12_VIDEO"],
    ["Blessings of Jw.org", "https://tv.jw.org/#en/mediaitems/pub-jwb_201705_13_VIDEO"],
    ["Better is the end of a matter", "https://tv.jw.org/#en/mediaitems/pub-jwb_201705_14_VIDEO"],
    ["They would be objects of Persecution", "https://tv.jw.org/#en/mediaitems/pub-jwb_201705_16_VIDEO"],
    ["Listen with understanding", "https://tv.jw.org/#en/mediaitems/pub-jwb_201706_10_VIDEO"],
    ["Let no one take you captive", "https://tv.jw.org/#en/mediaitems/pub-jwb_201706_11_VIDEO"],
    ["Morality in the last days", "https://tv.jw.org/#en/mediaitems/pub-jwb_201706_9_VIDEO"],
    ["Guard your relationship with Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201707_11_VIDEO"],
    ["Make decisions with Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201707_9_VIDEO"],
    ["Sacrifice with a willing attitude", "https://tv.jw.org/#en/mediaitems/pub-jwb_201708_10_VIDEO"],
    ["He used his body to honor Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201708_12_VIDEO"],
    ["Husbands, love your wife as yourself", "https://tv.jw.org/#en/mediaitems/pub-jwb_201708_8_VIDEO"],
    ["Humble or Haughty?", "https://tv.jw.org/#en/mediaitems/pub-jwb_201709_10_VIDEO"],
    ["Micah Waited on Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201709_11_VIDEO"],
    ["All Things To people of all sorts", "https://tv.jw.org/#en/mediaitems/pub-jwb_201709_9_VIDEO"],
    ["Starve Your Distraction, Feed Your Focus", "https://tv.jw.org/#en/mediaitems/pub-jwb_201710_10_VIDEO"],
    ["Use your talents wisely", "https://tv.jw.org/#en/mediaitems/pub-jwb_201710_11_VIDEO"],
    ["Elders Take the Lead!", "https://tv.jw.org/#en/mediaitems/pub-jwb_201710_8_VIDEO"],
    ["Cooperation builds unity", "https://tv.jw.org/#en/mediaitems/pub-jwb_201711_10_VIDEO"],
    ["Reasoning on Matters", "https://tv.jw.org/#en/mediaitems/pub-jwb_201711_8_VIDEO"],
    ["Cannot Slave for Two Masters", "https://tv.jw.org/#en/mediaitems/pub-jwb_201712_3_VIDEO"],
    ["Survive the Great Day", "https://tv.jw.org/#en/mediaitems/pub-jwb_201802_12_VIDEO"],
    ["Be Faithful Like Abraham", "https://tv.jw.org/#en/mediaitems/pub-jwb_201802_8_VIDEO"],
    ["Following Directions Saves Lives", "https://tv.jw.org/#en/mediaitems/pub-jwb_201803_11_VIDEO"],
    ["Be Acceptable to Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201803_7_VIDEO"],
    ["Be Forgiving", "https://tv.jw.org/#en/mediaitems/pub-jwb_201803_9_VIDEO"],
    ["Keep Promises, Receive Blessings", "https://tv.jw.org/#en/mediaitems/pub-jwb_201804_13_VIDEO"],
    ['"Take Your Stand Against Him"', "https://tv.jw.org/#en/mediaitems/pub-jwb_201804_7_VIDEO"],
    ["Can Endeavors Become Distractions?", "https://tv.jw.org/#en/mediaitems/pub-jwb_201806_3_VIDEO"],
    ['“Become Imitators of God”', "https://tv.jw.org/#en/mediaitems/pub-jwb_201807_10_VIDEO"],
    ["I Do Not Know You", "https://tv.jw.org/#en/mediaitems/pub-jwb_201807_11_VIDEO"],
    ["Be Prepared", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201808_10_VIDEO"],
    ["Guard Against Greed", "https://tv.jw.org/#en/mediaitems/pub-jwb_201809_9_VIDEO"],
    [" A Proper View of Discipline Keeps Us Close to Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201809_7_VIDEO"],
    ["Proper View of Discipline Keeps Us Close to Jehovah", "https://tv.jw.org/#en/mediaitems/pub-jwb_201809_7_VIDEO"],
    ["Lacking Good Sense", "https://tv.jw.org/#en/mediaitems/pub-jwb_201809_11_VIDEO"],
    ['"Keep on the Watch"', "https://tv.jw.org/#en/mediaitems/pub-jwb_201810_10_VIDEO"],
    ["God’s People Glorify His Name", "https://tv.jw.org/#en/mediaitems/pub-jwb_201810_4_VIDEO"],
    ["Neutral in the Ministry", "https://tv.jw.org/#en/mediaitems/pub-jwb_201810_12_VIDEO"],
    ['"Continue Putting Up With One Another"', "https://tv.jw.org/#en/mediaitems/pub-jwb_201811_7_VIDEO"],
    ["Strengthen Our Hope", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201811_11_VIDEO"],
    ["Testing Jehovah Out", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201812_2_VIDEO"],
    ["Draw Close to Jehovah", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201812_4_VIDEO"],
    ["This Is the Way", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201901_2_VIDEO"],
    ["Pride Is Before a Crash", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201902_12_VIDEO"],
    ["Jehovah Protects His People", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201903_7_VIDEO"],
    ["Have Humility and Peace", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201903_10_VIDEO"],
    ["Jehovah Guards Loyal Ones", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201903_12_VIDEO"],
    ["Show Humility", "https://tv.jw.org/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201904_6_VIDEO"],
    ["Abraham—Jehovah’s Friend", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201904_10_VIDEO"],
    ["Satan’s Crafty Strategy", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201904_12_VIDEO"],
    ["Inviting All to the Memorial", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201904_14_VIDEO"],
    ["Finding True Happiness", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201905_9_VIDEO"],
    ["Pursue the Way of Love", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201905_13_VIDEO"],
    ["A Balanced View of Guilt", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201907_9_VIDEO"],
    ["The Heart Is Treacherous", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201908_7_VIDEO"],
    ["Every Word From Jehovah", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201908_9_VIDEO"],
    ["Oppose the Devil", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201909_7_VIDEO"],
    ['"Do All Things for God’s Glory"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201909_9_VIDEO"],
    ["Discern Satan’s Lies", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201910_8_VIDEO"],
    ["Avoid Murmuring", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201910_10_VIDEO"],
    ["Jehovah Is Our Potter", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201911_10_VIDEO"],
    ["In Awe of Jehovah", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201911_12_VIDEO"],
    ["'Remember Those Taking the Lead'", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_201912_4_VIDEO"],
    ["Hope in the Resurrection", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202001_2_VIDEO"],
    ["Imitate Jehovah’s Generosity", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202001_26_VIDEO"],
    ["Jehovah’s Name Is Sanctified", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202002_9_VIDEO"],
    ['"Guard Against Every Sort of Greed"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202002_7_VIDEO"],
    ["Be Faithful Like Noah", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202002_11_VIDEO"],
    ["Comprehending the Heavenly Calling", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202003_10_VIDEO"],
    ["Be Clay in Jehovah’s Hands", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202003_14_VIDEO"],
    ['"Christ Died for Us"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202004_9_VIDEO"],
    ["Think God's Thoughts, Not Those of Men", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202004_13_VIDEO"],
    ['Jehovah—"The God of All Comfort"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202004_10_VIDEO"],
    ["Do You See Jehovah’s Hand?", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202004_7_VIDEO"],
    ["Avoid Flirting and Ego", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202004_12_VIDEO"],
    ["Moving Forward With Faith, Not Fear", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202004_14_VIDEO"],
    ['"Become One Flock"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202005_9_VIDEO"],
    ["Seek the Advantage of the Other Person", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202005_14_VIDEO"],
    ["Learn to Be Content", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202005_11_VIDEO"],
    ['Jehovah "Well Knows How We Are Formed"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202006_15_VIDEO"],
    ["If Misjudged, Remain Loyal", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202006_2_VIDEO"],
    ["Be Reasonable in the Family", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202006_5_VIDEO"],
    ["Protect Yourself From Lies", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202007_10_VIDEO"],
    ['"Ponder Over These Things"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202007_11_VIDEO"],
    ["Wisdom Is With the Modest Ones", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202009_10_VIDEO"],
    ['Make Known "the Good News"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202009_8_VIDEO"],
    ['Jehovah Will "Carry It Out"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202009_11_VIDEO"],
    ['"Draw Close to God"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202010_9_VIDEO"],
    ["Stand Firm Against Our Enemy", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202010_6_VIDEO"],
    ["Another Convenient Time", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202010_16_VIDEO"],
    ["Do I Forgive Others?", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202010_12_VIDEO"],
    ["Our Hope of the Resurrection", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202011_9_VIDEO"],
    ["Be a Discerning Student", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202011_7_VIDEO"],
    ["Strengthen Your Spiritual Core", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202011_11_VIDEO"],
    ["Our Witnessing Conquers Satan", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202012_9_VIDEO"],
    ["Learning a New Language", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202012_6_VIDEO"],
    ['"Have . . . Concern for One Another"', "https://www.jw.org/en/library/videos/#en/mediaitems/LatestVideos/pub-jwb_202012_11_VIDEO"],
    ["Faith -- Assured Expectation", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202101_12_VIDEO"],
    ['"Do Not Be Quick to Take Offense"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202101_13_VIDEO"],
    ['"Do Not Be Quick to Take Offense"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202101_13_VIDEO"],
    ['"Jehovah Our God Is One Jehovah"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202102_10_VIDEO"],
    ['"Ezra Had Prepared His Heart"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202102_11_VIDEO"],
    ["Keep the Mental Attitude of Christ", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202102_12_VIDEO"],
    ['Continue to "Be Transformed"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202103_9_VIDEO"],
    ['"They Will Become One Flesh"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202103_8_VIDEO"],
    ['Memorial Morning Worship— "The Spirit Itself Bears Witness With Our Spirit"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202103_13_VIDEO"],
    ['"Deaden . . . Your Body Members"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202103_12_VIDEO"],
    ['"Jehovah Raises Up the Meek"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202104_9_VIDEO"],
    ["Let Jehovah Mold You", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb_202104_7_VIDEO"],
    ["Why Reach Out?", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-080_12_VIDEO"],
    ["We Serve the God of True Prophecy", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-080_7_VIDEO"],
    ['"Take Courage!', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-080_13_VIDEO"],
    ["See the Big Picture", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-081_2_VIDEO"],
    ['"The Plans of the Diligent', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-082_9_VIDEO"],
    ["Jehovah Loves a Cheerful Giver", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-082_7_VIDEO"],
    ["Faithful With Material Things", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-082_10_VIDEO"],
    ["Comfort and Strengthen One Another", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-083_6_VIDEO"],
    ['"I Give You My Peace', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-083_11_VIDEO"],
    ["Obedience Is a Protection", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-083_10_VIDEO"],
    ["How to Be Resilient", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-084_7_VIDEO"],
    ['Tap Into "the Power Beyond What Is Normal"', "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-084_9_VIDEO"],
    ["Keep Spinning on the Potter’s Wheel", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-084_10_VIDEO"],
    ["Assisting Our Brothers Materially", "https://www.jw.org/en/library/videos/#en/mediaitems/VODPgmEvtMorningWorship/pub-jwb-085_2_VIDEO"]
]