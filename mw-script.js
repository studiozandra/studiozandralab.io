// reference: https://stackoverflow.com/a/44059520/9374498



function randomMW() {
  // state variable contains order and last index
  var state = null;
  // read state from localStorage
  if (localStorage.randomState) {
    state = JSON.parse(localStorage.randomState);
    
    // the # of items has changed, need to re-generate
    if (state.order.length != urlsList.length) {
      state = null;
      console.log("hey, changes")
    }
  }
  // missing or invalid state in localStorage
  if (!state) {
    // uninitialized state
    state = { order: [], index: 0 };
    
    // build a random order
    for (var i = 0; i < urlsList.length; i++) {
      state.order.splice(Math.floor(Math.random() * (i + 1)), 0, i);
    }
  }
  // get the current page based on saved state 
  var page = urlsList[state.order[state.index]];

  // get the current talk title based on saved state 
  var talkTitle = page[0];

  // increment the index, wrapping on overflow
  state.index = (state.index + 1) % urlsList.length;
  // save the updated state to localStorage
  localStorage.randomState = JSON.stringify(state);
  
  // show title
  document.getElementById("link_name").innerHTML = talkTitle;
  console.log(page[0])

  // redirect the user
  location.href = page[1];
  
}